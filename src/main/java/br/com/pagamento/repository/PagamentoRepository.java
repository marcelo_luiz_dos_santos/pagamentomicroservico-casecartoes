package br.com.pagamento.repository;

import br.com.pagamento.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findAllByCartaoId(int id);
}
