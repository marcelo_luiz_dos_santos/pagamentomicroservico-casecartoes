package br.com.pagamento.service;

import br.com.pagamento.Client.Cartao;
import br.com.pagamento.Client.CartaoClient;
import br.com.pagamento.model.Pagamento;
import br.com.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.RuntimeErrorException;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento criarPagamento(Pagamento pagamento){
            Cartao cartao = new Cartao();
            cartao = cartaoClient.buscarPorIdAndAtivo(pagamento.getCartaoId(), true);
            Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
            return pagamentoObjeto;
    }

    public Iterable<Pagamento> buscarTodosPorId(int id){
        Iterable<Pagamento> Pagamentos = pagamentoRepository.findAllByCartaoId(id);
        return Pagamentos;
    }

}