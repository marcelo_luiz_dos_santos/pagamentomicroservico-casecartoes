package br.com.pagamento.controller;

import br.com.pagamento.model.Pagamento;
import br.com.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento criarPagamento(@RequestBody @Valid Pagamento pagamento) {
            Pagamento pagamentoObjeto = pagamentoService.criarPagamento(pagamento);
            return pagamentoObjeto;
    }

    @GetMapping("/pagamentos/{idCartao}")
    public Iterable<Pagamento> buscarTodosPorIdCartao(@PathVariable(name = "idCartao") int id){
        Iterable<Pagamento> pagamentos = pagamentoService.buscarTodosPorId(id);
        return pagamentos;
    }
}
