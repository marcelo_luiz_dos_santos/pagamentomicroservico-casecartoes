package br.com.pagamento.Client;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoClientDecoder implements ErrorDecoder {
    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 400) {
            return new CartaoNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }
}
