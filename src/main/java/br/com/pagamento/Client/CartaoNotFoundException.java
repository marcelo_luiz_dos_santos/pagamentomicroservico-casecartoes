package br.com.pagamento.Client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cartão não encontrado ou não ativo.")
public class CartaoNotFoundException extends RuntimeException {
}
