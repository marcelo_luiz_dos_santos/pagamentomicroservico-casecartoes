package br.com.pagamento.Client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CARTAO", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cartao/{id}/{ativo}")
    Cartao buscarPorIdAndAtivo(@PathVariable int id, @PathVariable Boolean ativo);


}
