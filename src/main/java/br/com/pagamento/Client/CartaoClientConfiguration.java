package br.com.pagamento.Client;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CartaoClientConfiguration {
    @Bean
    public ErrorDecoder getCartaoClientDecoder() {
        return new CartaoClientDecoder();
    }
}
